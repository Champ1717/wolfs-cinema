package com.app.banuelos.wolfscinema;

/**
 * Created by Student on 9/27/2016.
 */
public class Predictions {

    public static Predictions predictions;

    private Predictions() {

    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }
}
